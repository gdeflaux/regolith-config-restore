# Regolith Config Restore Script

This script allows you to restore your Regolith config files.

It relies on the fact that:

- You had previously staged your config files in the `$HOME/.config/regolith` directory.
- You turned that directory into a git repository and that it ignores the `flags` directory (see [below](#creating-git-repository-for-your-config-files)).

This script will clone that git repository and add its content to the `$HOME/.config/regolith` directory. You can then continue to update and version control your config files in `$HOME/.config/regolith`. If your `.Xresources-regolith` file is also in that repository, it will create a symbolic link to it from `$HOME/.Xresources-regolith`.

**You should only run this script on a non staged Regolith Linux**, i.e. the `$HOME/.config/regolith` directory should only contain the `flags` directory.

Example of `$HOME/.config/regolith` directory:

```
/home/guillaume/.config/regolith
├── flags
│   ├── first-time-setup-r1-3
│   ├── show-shortcuts
│   └── ui-fingerprint
├── .git
├── .gitignore
├── i3
│   └── config
├── i3xrocks
│   └── config
├── README.md
├── styles
│   ├── gnome
│   ├── i3-wm
│   ├── i3xrocks
│   ├── rofi
│   └── st-term
├── wallpaper.jpg
└── .Xresources-regolith
```

## Usage

```
Usage: regolith-config-restore.sh <repository>
	repository  A valid repository URL for 'git clone' (ssh or https)
```

### Cloning the repo

Example:

```
$ git clone https://gitlab.com/gdeflaux/regolith-config-restore
$ cd regolith-config-restore
$ ./regolith-config-restore.sh https://gitlab.com/gdeflaux/regolith-config-restore.git
```

Replace `https://gitlab.com/gdeflaux/regolith-config-restore.git` by the URL of your repo.

### Without cloning the repo

This method will **not** leave a copy of the script on your hard drive. Example:

```
$ source <(curl -s https://gitlab.com/gdeflaux/regolith-config-restore/-/raw/master/regolith-config-restore.sh) "https://gitlab.com/gdeflaux/regolith-config-restore.git"
```

Replace `https://gitlab.com/gdeflaux/regolith-config-restore.git` by the URL of your repo.

## Creating git repository for your config files

This commands will create a repository that can be used by the install script to restore the config files.

```
$ cd $HOME/.config/regolith
$ git init .
$ echo "flags/" > .gitignore
$ git add .
$ git commit -m "initial commit"
```

You should now `push` to a remote git repository (ex: Github, Gitlab).
