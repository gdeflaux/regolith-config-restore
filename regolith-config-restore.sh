#!/bin/bash

# Exit codes
SUCCESS=0
ERR_USAGE=1
ERR_CLONING_FAILED=2
ERR_FLAGS_FAILED=3
ERR_BACKUP_FAILED=4
ERR_REPLACE_FAILED=5
ERR_SYMLINK_FAILED=6
ERR_UNSTAGED_INSTALL=7

# Variables
REPO_URL=$1
TMP_DIR=/tmp/regolith-config
DOT_CONFIG_DIR=$HOME/.config

function usage() {
  echo "Usage: $0 <repository>"
  echo -e "\trepository  A valid repository URL for 'git clone' (ssh or https)"
  exit $ERR_USAGE
}

function is_unstaged_install {
  A=`ls -1 $DOT_CONFIG_DIR/regolith | wc -l`
  B=`ls -1 $DOT_CONFIG_DIR/regolith`

  if [ $A -eq 1 ] && [ $B == "flags" ]
  then
    return 1
  fi

  return 0
}

function err_display {
  echo ""
  echo "     $1"
  echo "       $2"
  echo ""
  exit $3
}

function install {
  echo ""
  echo "Ok let's do it!"
  echo ""

  echo "Repo: $REPO_URL"
  echo ""

  rm -rf $TMP_DIR

  echo "  1. Cloning repo to '$TMP_DIR'"
  A=`git clone --quiet $REPO_URL $TMP_DIR 2>&1`
  if [ $? -ne 0 ]; then err_display "Cloning failed:" "$A" $ERR_CLONING_FAILED; fi

  echo "  2. Copying 'flags' dir to repo"
  A=`cp -r $DOT_CONFIG_DIR/regolith/flags $TMP_DIR 2>&1`
  if [ $? -ne 0 ]; then err_display "Copying failed:" "$A" $ERR_FLAGS_FAILED; fi

  echo "  3. Backuping 'regolith' dir to 'regolith-orig'"
  A=`mv $DOT_CONFIG_DIR/regolith $DOT_CONFIG_DIR/regolith-orig 2>&1`
  if [ $? -ne 0 ]; then err_display "Backup failed:" "$A" $ERR_BACKUP_FAILED; fi

  echo "  4. Replacing 'regolith' dir with repo"
  A=`mv $TMP_DIR $DOT_CONFIG_DIR/regolith 2>&1`
  if [ $? -ne 0 ]; then err_display "Replace failed:" "$A" $ERR_REPLACE_FAILED; fi

  if [ -a $DOT_CONFIG_DIR/regolith/.Xresources-regolith ]
  then
    echo "  5. Creating symlink for the '.Xresource-regolith' file"
    rm -f $HOME/.Xresources-regolith
    A=`ln -s $DOT_CONFIG_DIR/regolith/.Xresources-regolith $HOME/.Xresources-regolith 2>&1`
    if [ $? -ne 0 ]; then err_display "Symlink creation failed:" "$A" $ERR_SYMLINK_FAILED; fi
  fi

  echo ""
  echo "Done!"
}

if [ $# -ne 1 ]
then
  usage
fi

echo "####################################"
echo "## REGOLITH CONFIG RESTORE SCRIPT ##"
echo "####################################"
echo ""

is_unstaged_install
if [ $? -eq 0 ]
then
  echo "!! You should only use this script on an unstaged Regolith Linux !!"
  exit $ERR_UNSTAGED_INSTALL
fi

while [ true ]
do
  read -p "Proceed [y/n]? " CHOICE

  case $CHOICE in
    y)
      install
      break
      ;;
    n)
      echo "Ok. Goodbye!"
      break
      ;;
    *)
      echo ""
      echo "  Say that again, please!"
      echo ""
      ;;
  esac
done

exit $SUCCESS
